//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Nancy on 8/21/12.
//  Copyright (c) 2012 Nancy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject
- (void) pushOperand:(double)operand;
- (double)performOperation:(NSString *) operation;
- (NSString*) checkAnswer:(int)calculatedAnswer;

@end
