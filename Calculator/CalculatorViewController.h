//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Nancy on 8/21/12.
//  Copyright (c) 2012 Nancy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *op1Display;
@property (weak, nonatomic) IBOutlet UILabel *op2Display;
@property (weak, nonatomic) IBOutlet UILabel *answerDisplay;
@property (weak, nonatomic) IBOutlet UILabel *resultDisplay;

@end
