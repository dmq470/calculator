//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Nancy on 8/21/12.
//  Copyright (c) 2012 Nancy. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController ()
@property (nonatomic) BOOL userIsInTheMiddleOfEnteringANumber;
@property (nonatomic, strong) CalculatorBrain *brain;
@end

@implementation CalculatorViewController

@synthesize op1Display = _display;
@synthesize op2Display = _display2;
@synthesize answerDisplay = _answerDisplay;
@synthesize resultDisplay = _resultDisplay;
@synthesize userIsInTheMiddleOfEnteringANumber = _userIsInTheMiddleOfEnteringANumber;
@synthesize brain = _brain;

- (CalculatorBrain *) brain
{
    if ( !_brain )  _brain = [[CalculatorBrain alloc] init];
    return _brain;
}

- (IBAction)digitPressed:(UIButton *)sender {
    
    NSString *digit = sender.currentTitle;
    
    if (self.userIsInTheMiddleOfEnteringANumber)
    {
        self.answerDisplay.text = [self.answerDisplay.text stringByAppendingString:digit];
    }
    else
    {
        self.answerDisplay.text = digit;
        self.userIsInTheMiddleOfEnteringANumber = YES;
    }
}

int answer;

- (int)getRandomNum {
    int randOperand1 = arc4random() % 10;
    return randOperand1;
}

- (IBAction)nextPressed {
    int randOperand1;
    randOperand1 = [self getRandomNum];
    NSString *resultString = [NSString stringWithFormat:@"%i", randOperand1];
    self.op1Display.text = resultString;
    
    int randOperand2 = [self getRandomNum];
    NSString *resultString2 = [NSString stringWithFormat:@"%i", randOperand2];
    self.op2Display.text = resultString2;
    
    answer = randOperand1 * randOperand2;
}

- (IBAction)enterPressed {
    
    [self.brain pushOperand:[self.answerDisplay.text doubleValue]];
    self.resultDisplay.text = [self.brain checkAnswer:answer];
    
    self.userIsInTheMiddleOfEnteringANumber = NO;
}

- (IBAction)operationPressed:(UIButton *)sender {
    if (self.userIsInTheMiddleOfEnteringANumber) [self enterPressed];
    
    double result = [self.brain performOperation:sender.currentTitle];
    NSString *resultString = [NSString stringWithFormat:@"%g", result];
    self.op1Display.text = resultString;
}
- (void)viewDidUnload {
    [self setOp2Display:nil];
    [self setAnswerDisplay:nil];
    [self setResultDisplay:nil];
    [super viewDidUnload];
}
@end
